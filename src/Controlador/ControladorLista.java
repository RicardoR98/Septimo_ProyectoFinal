package Controlador;

import Factory.Factory;
import Modelo.DAO;
import Modelo.ModeloLista;
import Modelo.ModeloPrincipal;
import Singleton.Singleton;
import Vista.VistaLista;
import Vista.VistaPrincipal;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

public class ControladorLista implements ActionListener {

    /*Declaracion de las variables de clase para poderlos utilizar en la clase*/
    private final JPanel panel = new JPanel();
    private final VistaLista vista;
    private final ModeloLista modelo;
    /*Declaracion de un modelo para una tabla para poderlo utilizar más adelante*/
    private DefaultTableModel dtm;
    ResultSet rs = null;

    /*Constructor que inicializa la vista y el modelo de la clase para su correcto funcionamiento*/
    public ControladorLista(VistaLista vista, ModeloLista modelo) {
        this.vista = vista;
        this.modelo = modelo;
    }

    /*Metodo que inicaliza la vista para que se pueda visualizar a el usurio*/
    public void iniciar() throws ClassNotFoundException, SQLException {
        vista.setTitle("Lista");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);

        /*Desactivacion de algunos elementos en la vista para no utilizarlos intencionalmente*/
        vista.getBtn_guardar().setEnabled(false);
        vista.getTf_Nombre().setEnabled(false);
        vista.getTf_Telefono().setEnabled(false);
        vista.getTf_Correo().setEnabled(false);
        vista.getTf_Celular().setEnabled(false);

        /*Llamado a metodo de llenarTabla para poder consultar los elementos de la base de datos y visualizarlos*/
        llenarTabla();
        asignarControl();
    }

    /*Asignamos el control a los elemntos de la vista para que puedan funcionar*/
    private void asignarControl() {
        vista.getTf_Celular().addActionListener(this);
        vista.getTf_Correo().addActionListener(this);
        vista.getTf_Nombre().addActionListener(this);
        vista.getTf_Telefono().addActionListener(this);
        vista.getTf_id().addActionListener(this);
        vista.getBtn_consulta().addActionListener(this);
        vista.getBtn_eliminar().addActionListener(this);
        vista.getBtn_guardar().addActionListener(this);
        vista.getBtn_regresar().addActionListener(this);
    }

    /*Metodo que inicializara a la tabla con los elementos de la base de datos para poderlos visualizar a el usuario*/
    private void llenarTabla() throws ClassNotFoundException, SQLException {
        /*Llamando a metodo de la clase Factory para que nos regrese la instancia de la clase ListaDAO para poderla usar*/
        DAO p = Factory.extender(1, Singleton.crearConexion());

        /*Creacion de arreglo para las columnas que contendra la tabla, identificando que es cada uno de los dats visualizados*/
        String[] columnas = {"Identificador", "Nombre", "Telefono", "Correo", "Celular"};
        /*Asignando las columnas a la tabla diciendole que tendra 0 filas  un inicio*/
        dtm = new DefaultTableModel(columnas, 0);

        /*Mandando cadena de consulta a el metodo abstracto consultar para poder visualizar cada uno de los registros que esten dados de alta, 
        y guardandolos en una variable de ResultSet para poderlos imprimir a el usuario en la tabla*/
        rs = p.consultar("select * from registros");

        /*Ciclo While que dara vueltas a el resultado hasta que la variable sea falsa, hasta entonces dara ciclos para imprimir cada uno de los datos*/
        while (rs.next()) {

            /*Asignacion de un arreglo con la cantdad de datos que imprimiremos en la tabla*/
            Object row[] = new Object[5];

            /*le asignamos al rreglo cada uno de los datos que tenga la base de datos, mandandoles a llamar por medio del mismo nombre de la columna
            que se tenga en la base de datos*/
            row[0] = rs.getInt("id");
            row[1] = rs.getString("nombre");
            row[2] = rs.getString("telefono");
            row[3] = rs.getString("correo");
            row[4] = rs.getString("celular");

            /*Se le asigna el arreglo a el modelo para que la imprima en la siguiente fila*/
            dtm.addRow(row);
        }

        /*Le asignamos el modelo a la tabla que esta en la vista del usuario para que los pueda visualizar*/
        vista.getTb_Lista().setModel(dtm);
    }

    /*Metodo que detectara cada uno de los eventos que se realizen en la vista*/
    @Override
    public void actionPerformed(ActionEvent e) {
        /*If que detecta si es que fue clickeado el boton eliminar*/
        if (e.getSource() == vista.getBtn_eliminar()) {
            /*If que verifica si los datos de el TextFiel para ingresar un id no viene vacio, y si es así desplegarle un mensaje*/
            if (vista.getTf_id().getText().equals("")) {
                JOptionPane.showMessageDialog(panel, "Algun campo se encuentra vacio, favor de complementarlos", "Warning", JOptionPane.WARNING_MESSAGE);
            } else {
                /*Desactivando los botones consulta y eliminar para que no los presiones por accidente una vez empezado el evento*/
                vista.getBtn_consulta().setEnabled(false);
                vista.getBtn_eliminar().setEnabled(false);

                try {
                    /*Instancia de la clase Factory para poder traer una instancia de la clase ListaDAO para poder usar sus metodos abstractos de 
                    la clase DAO*/
                    DAO p = Factory.extender(2, Singleton.crearConexion());

                    /*Le asignamos al modelo el id para que lo pueda usar a clase*/
                    modelo.setIdentificador(Integer.parseInt(vista.getTf_id().getText()));

                    /*Le mandamos el modelo al metodo abstracto remove para poder realizar alguna accion con el dato*/
                    p.remove(modelo);
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(ControladorLista.class.getName()).log(Level.SEVERE, null, ex);
                }

                /*Se activan de nuevo los botones para que los pueda utilizar el usuario una vez terminada la accion en la base de datos*/
                vista.getBtn_consulta().setEnabled(true);
                vista.getBtn_eliminar().setEnabled(true);

                try {
                    /*Borramos la tabla y la llenamos de nuevo*/
                    dtm = null;
                    llenarTabla();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(ControladorLista.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } 
        /*Detectamos el evento click de el boton regresar*/
        else if (e.getSource() == vista.getBtn_regresar()) {
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    /*Se le manda el modelo y la vista a la clase controladorPrincipal para poder visualizar su vista*/
                    new ControladorPrincipal(new VistaPrincipal(), new ModeloPrincipal()).iniciar();
                    /*Desaparecemos la vista actual para que no esten abiertas dos vistas al mismo tiempo*/
                    vista.dispose();
                }
            });
        } 
        /*Detectamos el evento click en el boton consultar*/
        else if (e.getSource() == vista.getBtn_consulta()) {

            /*If que hara que no mandemos datos vacios a la base de datos*/
            if (vista.getTf_id().getText().equals("")) {
                JOptionPane.showMessageDialog(panel, "Algun campo se encuentra vacio, favor de complementarlos", "Warning", JOptionPane.WARNING_MESSAGE);
            } else {
                try {
                    /*Instanciamos a la clase Factory para poder utilizar la clase ListaDAO para poderle mandar datos y realize alguna acción*/
                    DAO p = Factory.extender(2, Singleton.crearConexion());

                    /*Mandamos una consulta a la base de datos, y esperamos a recibir na respuesta de la misma para poder utilizar esos datos*/
                    rs = p.consultar("select * from registros where id = " + Integer.parseInt(vista.getTf_id().getText()));

                    /*If que checara si la respuesta de la base de datos trae datos*/
                    if (rs.first() != false) {
                        /*Mandamos una consulta a la base de datos, y esperamos a recibir na respuesta de la misma para poder utilizar esos datos*/
                        rs = p.consultar("select * from registros where id = " + Integer.parseInt(vista.getTf_id().getText()));
                        /*Ciclamos la respuesta de la base de datos para poder obtener cada uno de los datos que la misma nos responda*/
                        while (rs.next()) {

                            /*Cada uno de los datos que se reciban de la base de datos, sera impresa en los textFiel para poderlos modificar a 
                            conveniencia del usuario*/
                            vista.getTf_Nombre().setText(rs.getString("nombre"));
                            vista.getTf_Telefono().setText(rs.getString("telefono"));
                            vista.getTf_Correo().setText(rs.getString("correo"));
                            vista.getTf_Celular().setText(rs.getString("celular"));

                        }
                        /*Activamos y desactivamos los componentes de la vista para evitar errores a la hora de realizar la accion*/
                        vista.getBtn_guardar().setEnabled(true);
                        vista.getTf_id().setEditable(false);
                        vista.getTf_Nombre().setEnabled(true);
                        vista.getTf_Telefono().setEnabled(true);
                        vista.getTf_Correo().setEnabled(true);
                        vista.getTf_Celular().setEnabled(true);
                        vista.getBtn_consulta().setEnabled(false);
                        vista.getBtn_eliminar().setEnabled(false);
                    } 
                    /*En caso de que traiga una respuesta vacia desplegamos un mensaje*/
                    else {
                        JOptionPane.showMessageDialog(panel, "No existe el registro", "Warning", JOptionPane.WARNING_MESSAGE);
                    }

                } catch (SQLException | ClassNotFoundException ex) {
                    Logger.getLogger(ControladorLista.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } 
        /*If que detecta el click en el boton guardar*/
        else if (e.getSource() == vista.getBtn_guardar()) {
            /*If que verifica que no mandemos datos vacios a la base de datos*/
            if (vista.getTf_Nombre().getText().equals("")
                    || vista.getTf_Correo().getText().equals("") || vista.getTf_Celular().getText().equals("")) {
                JOptionPane.showMessageDialog(panel, "Algun campo se encuentra vacio, favor de complementarlos", "Warning", JOptionPane.WARNING_MESSAGE);
            } else {
                /*Desactivamos el boton guardar para no realizar la acción más de una vez*/
                vista.getBtn_guardar().setEnabled(false);

                try {
                    /*Instancia a la clase Factory para poder traer una instancia de la clase ListaDAO para poderle mandar datos*/
                    DAO p = Factory.extender(2, Singleton.crearConexion());

                    /*Asignando los datos a el modelo para poderlos mandar*/
                    modelo.setIdentificador(Integer.parseInt(vista.getTf_id().getText()));
                    modelo.setNombre(vista.getTf_Nombre().getText());
                    modelo.setTelefono(vista.getTf_Telefono().getText());
                    modelo.setCorreo(vista.getTf_Correo().getText());
                    modelo.setCelular(vista.getTf_Celular().getText());

                    /*Mandamos el modelo a el metodo edit para que realice las acciones correspondientes*/
                    p.edit(modelo);

                    /*Borramos la tabla y la volvemos a llenar*/
                    dtm = null;
                    llenarTabla();
                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(ControladorPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }

                /*Vaciando cada uno de los editText para mayor comodidad del usuario*/
                vista.getTf_Nombre().setText("");
                vista.getTf_Telefono().setText("");
                vista.getTf_Correo().setText("");
                vista.getTf_Celular().setText("");
                vista.getTf_id().setText("");

                /*Activando y desactivando los elementos de la vista para el usuario*/
                vista.getBtn_guardar().setEnabled(false);
                vista.getTf_Nombre().setEnabled(false);
                vista.getTf_Telefono().setEnabled(false);
                vista.getTf_Correo().setEnabled(false);
                vista.getTf_Celular().setEnabled(false);
                vista.getTf_id().setEditable(true);
                vista.getBtn_consulta().setEnabled(true);
                vista.getBtn_eliminar().setEnabled(true);
            }
        }
    }

}
