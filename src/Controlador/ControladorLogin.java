package Controlador;

import Factory.Factory;
import Modelo.DAO;
import Modelo.ModeloLogin;
import Modelo.ModeloPrincipal;
import Singleton.Singleton;
import Vista.VistaLogin;
import Vista.VistaPrincipal;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class ControladorLogin implements ActionListener {

    /*Declaracion de variables de vistaLogin y ModeloLogin, con el ResultSet para peticiones a MySql*/
    private final JPanel panel = new JPanel();
    private final VistaLogin vista;
    private final ModeloLogin modelo;
    ResultSet rs = null;

    /*Controlador que recibira la vista y el modelo para que pueda funcionar la clase*/
    public ControladorLogin(VistaLogin vista, ModeloLogin modelo) {
        this.vista = vista;
        this.modelo = modelo;
    }

    /*Metodo iniciar que se manda a hablar desde la instancia de la clase para poder inicializar la vista*/
    public void iniciar() {
        vista.setTitle("Login");
        vista.setLocationRelativeTo(null);
        vista.setVisible(true);
        asignarControl();
    }

    /*Metodo que asigna el control a los controles de la vistaLogin como botones y editText para poderlos utilizar en esta clase*/
    private void asignarControl() {
        vista.getTf_Usuario().addActionListener(this);
        vista.getTf_contrasena().addActionListener(this);
        vista.getBtn_Iniciar().addActionListener(this);
        /*Asignando un mensaje a un label para mostrarlo en la vista de el login*/
        vista.getl_mensajes().setText("Introduzca sus credenciales");
    }

    /*Metodo que detectara cada uno de los eventos que se realicen en la vistaLogin y aqui se podra decidir que hacer con cada uno de ellos*/
    @Override
    public void actionPerformed(ActionEvent e) {
        /*Detectamos el evento click del boton Iniciar*/
        if (e.getSource() == vista.getBtn_Iniciar()) {
            
            /*Validacion para no poder mandar datos vacios*/
            if (vista.getTf_Usuario().getText().equals("") || vista.getTf_contrasena().getText().equals("")) {
                JOptionPane.showMessageDialog(panel, "Ingrese sus credenciales", "Warning", JOptionPane.WARNING_MESSAGE);
            }else{
                try {
                    /*Instancia a la clase ModeloLogin para poder mandar el usuario y la contraseña*/
                    ModeloLogin ml = new ModeloLogin();
                    /*Asignamos la contraseña y el usuario a su respectivo Set para que la clase se encargue de ella*/
                    ml.setUsuario(vista.getTf_Usuario().getText());
                    ml.setContrasena(vista.getTf_contrasena().getText());
                    
                    /*Instanciamos la clase DAO, mandandole a llamar a la clase Factory para que regrese la clase completa que se le esta pidiendo*/
                    DAO p = Factory.extender(1, Singleton.crearConexion());
                    
                    /*mandamos a llamar al metodo consultar de la interface DAO por medio de la clase Factory y le mandamos una consulta para que 
                    nos regrese datos de la base de datos*/
                    rs = p.consultar("select id from usuarios where usuario = '" + ml.getUsuario() + "' and contrasena = '" + ml.getContrasena() + "'");
                    
                    System.out.println(rs.first());
                    
                    /*Realizamos un chequeo de si la respuesta de la base de datos es verdadera le dara acceso a el sistema*/
                    if (rs.first() != false) {
                        EventQueue.invokeLater(new Runnable() {
                            @Override
                            public void run() {
                                /*Construccion de la clase controladorPrincipal, mandandole como parametros la vista y el modelo de Principal
                                mandando a llamar al metodo inciar*/
                                new ControladorPrincipal(new VistaPrincipal(), new ModeloPrincipal()).iniciar();
                                /*Evento para poder cerrar la vista actual y poder visualizar solo una vista  ala vez*/
                                vista.dispose();
                            }
                        });
                    }
                    /*Si la respuesta es falsa se le mostrara un mensaje que sus credenciales son incorrectas y que las ingrese de nuevo*/
                    else {
                        JOptionPane.showMessageDialog(panel, "Error en sus credenciales", "Warning", JOptionPane.WARNING_MESSAGE);
                    }

                } catch (ClassNotFoundException | SQLException ex) {
                    Logger.getLogger(ControladorLogin.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
    }

}
