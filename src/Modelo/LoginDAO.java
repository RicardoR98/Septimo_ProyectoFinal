/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Singleton.Singleton;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rodri
 */
public class LoginDAO extends DAO<ModeloLogin> {

    public LoginDAO(Singleton conexion) {
        super(conexion);
    }

    @Override
    public void add(ModeloLogin obj) {}

    @Override
    public void edit(ModeloLogin obj) {}

    @Override
    public void remove(ModeloLogin obj) {}

    @Override
    public ResultSet consultar(String consulta) {
        try {
            ps = conexion.obtenerConexion().prepareStatement(consulta);
            rs = ps.executeQuery();
            return rs;
        } catch (SQLException e) {
            Logger.getLogger(PrincipalDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return null;
    }
    
}
